module.exports = function(system){


	return {
    	name: "INTEGRATION TEST",
    	process: function(params){
			system.aop.filter.add("avail:param:date", function(date){
				return date  + "--";
			});


			system.aop.filter.add("avail:param:date", function(date){
				return "++" + date;
			});


			system.aop.filter.add("avail:param:location", function(location){
				return location + "LOCATION";
			});

			system.aop.action.add("avail:before", function(){
				system.log.info("AVAIL:BEFORE");
			});


			system.aop.filter.add("avail:response:json", function(json){
				json.time = new Date();
				return json;
			});
	    }
	}
}

