var colors = require('colors/safe');

module.exports = function(){

  var log = function(level, str){
    console.log(colors[levels[level]]("["+level+"] ", str));
  };

  var levels = {
    "debug":"white",
    "info": "blue",
    "warn": "yellow",
    "error": "red"
  };

  var response  = {};
  Object.keys(levels).forEach(function (severity) {
    this[severity] = function (message) {
      log(severity, message);
    };
  }, response);

  return response;
};
