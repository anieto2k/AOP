module.exports = function(){

  var filters = {};
  var actions = {};

  return  {
    filter: {
      add: function(where, fn){
        if (typeof filters[where] == 'undefined') filters[where] = new Array();
	      filters[where].push(fn);
      },
      del: function(where, fn){
        if (typeof filters[where] == 'undefined') return;
      	var j = 0;
      	while (j < filters[where].length) {
      		if (filters[where][j] == fn) { filters[where].splice(j, 1);}
      		else { j++; }
      	}
      },
      apply: function(where, param){
        if (typeof filters == 'undefined') return param;
        if (typeof filters[where] == 'undefined') return param;
        for (var x=0; x<filters[where].length; x++)
          param = filters[where][x].apply(null, [param]);
        return param;
      }
    },
    action: {
      add: function(where, fn){
        if (typeof actions[where] == 'undefined') actions[where] = new Array();
	      actions[where].push(fn);
      },
      del: function(where, fn){
        if (typeof actions[where] == 'undefined') return;
      	var j = 0;
      	while (j < actions[where].length) {
      		if (actions[where][j] == fn) { actions[where].splice(j, 1);}
      		else { j++; }
      	}
      },
      do: function(where){
        if (typeof actions == 'undefined') return;
        if (typeof actions[where] == 'undefined') return;
        for (var x=0; x < actions[where].length; x++)
          actions[where][x].apply(null, []);
      }
    }
  }

}
