var path = require("path")
  , fs = require("fs")
  , express = require('express');

//var validMethods = ['GET', 'POST'];
module.exports = function(app){

  var system = {};

  // Incluimos la configuración
  system.config = require("./config/" + (process.env.ENV || 'LOCAL' + ".json"));

  // Incluimos AOP
  system.aop = require("./utils/aop.js")();

  // Incluimos logger
  system.log = require("./utils/logger.js")();

  // Incluimos DB
  //system.db = require("./utils/mongodb.js");


  // Cargamos los plugins 
  // TODO: Cargar plugins desde BD
  var plugins = require('./plugins/test/plugin.js')(system);
	plugins.process();
  
  // Inicializamos el router
  var router = express.Router();

  // Recorremos routes disponibles en /rest
  var dir = path.join(__dirname, "rest");
	fs.readdirSync(dir)
	    .filter(function(file) { return file.substr(-3) == '.js'; })
	    .forEach(function(file){
	      try {
	      	// iniciamos subroute
	      	var subrouter = express.Router();

	      	// Cargamos acciones disponibles
	        var actions = require(path.join(dir,file))(system);

	        // Recorremos las acciones disponibles

	        var root = file.replace(".js", '');

	        actions.forEach(function(action){
				// recorremos los métodos disponibles
				action.methods.forEach(function(method){
					system.log.info("[" + method + "] " + action.route);

					// Damos de alta el endpoint nuevo
	        		subrouter[method.toLowerCase()](action.route, function(req, res, next){
	        			var params = req.params;
	        			system.aop.action.do(root + ":before");

	        			action.process(params, function(err, data){
	        				if (err) {
	        					system.log.error(err);
	        					data = err;
	        				}

	        				res.json(system.aop.filter.apply(root + ":response:json",data));
		        			system.aop.action.do(root + ":after");
	        			});
	        		});
				});
	        });

	        router.use("/" + root, subrouter);	        
          	system.log.info("Cargamos: " + file);
	      }catch(e){
	        system.log.error("Error al cargar el controller: " + file);
	        console.error(e);
	      }
	  });


	return router;

}