var utils = require("../utils/utils.js");

module.exports = function(system){

  system.log.debug("Estamos dentro");

  return [
	{
		route: "/:date/:location",
		methods: ['GET', 'POST'],
		process: function(params, cb){
			var params = params || {};
			

		    if (params.date && params.location){
			    params.date = system.aop.filter.apply("avail:param:date", utils.sanitize(params.date));
	    		params.location = system.aop.filter.apply("avail:param:location", utils.sanitize(params.location));
				cb(null, params);
			} else {
				cb({code: "PARAM", error: "No params found"}, null);
			}

		}
	}
  ];
}
